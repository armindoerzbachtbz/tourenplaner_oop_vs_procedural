package ch.tbz.tourenplaner.menu;

import java.util.Date;

/**
 * This is just a class used to hold the information to display by CMenu for a Station
 */
public class CStationDetails {
    public int place;
    public Date arrivaltime;
    public Date departuretime;

    public int getPlace() {
        return place;
    }

    public Date getArrivaltime() {
        return arrivaltime;
    }

    public Date getDeparturetime() {
        return departuretime;
    }

    public CStationDetails(int place, Date arrivaltime, Date departuretime) {
        this.place = place;
        this.arrivaltime = arrivaltime;
        this.departuretime = departuretime;
    }
}
