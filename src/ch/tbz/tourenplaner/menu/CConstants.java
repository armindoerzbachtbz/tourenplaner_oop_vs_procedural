package ch.tbz.tourenplaner.menu;

import java.text.SimpleDateFormat;

/**
 * Storing Constants
 */
public class CConstants {
    /**
     * Date format used in the input output of the application
     */
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
}
