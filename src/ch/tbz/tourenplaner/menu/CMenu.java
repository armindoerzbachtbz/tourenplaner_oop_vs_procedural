package ch.tbz.tourenplaner.menu;


import java.text.ParseException;
import java.util.*;

/**
 * This class is doing the text ui input output stuff
 */
public class CMenu {
    /**
     * This displays the main menu and reads the input from console
     * @return choice or -1 for a wrong input
     */
    public static int mainmenu() {
        System.out.println("Main Menu");
        System.out.println("---------");
        System.out.println("0 Exit");
        System.out.println("1 Add Bus Station");
        System.out.println("2 Add Tour");
        System.out.println("3 List Tours");
        System.out.print("Your Choice:");
        try {
            return Integer.parseInt(new Scanner(System.in).nextLine());
        } catch (NumberFormatException ex) {
            return -1;
        }
    }

    /**
     * This is the menu for adding a Place.
     *
     * @return String[0]=plz String[1]=name
     */
    public static String[] addPlace() {
        System.out.println("Add a new Place");
        System.out.println("---------------");
        System.out.println("Enter the postal code:");
        String plz = new Scanner(System.in).nextLine();
        System.out.println("Enter the name of the place");
        String name = new Scanner(System.in).nextLine();
        String[] strings = {plz, name};
        return strings;

    }

    /**
     * This is the menu for adding a Tour
     * @return ArrayList of station details
     */
    public static ArrayList<CStationDetails> addTour(ArrayList<String[]> places) {
        System.out.println("Add a New tour");
        System.out.println("--------------");

        ArrayList<CStationDetails> stations = new ArrayList<>();
        int choice;
        do {
            System.out.println("The list if possible places are:");
            for (int i = 0; i < places.size(); i++) {
                System.out.printf("%2d %10s %30s\n", i + 1, places.get(i)[0], places.get(i)[1]);
            }
            System.out.printf("Choose place for the %d. Station (0 for exit):", stations.size() + 1);
            try {
                choice = Integer.parseInt(new Scanner(System.in).nextLine());
                Date arrivaltime = null;
                Date departuretime = null;
                if (choice > 0 && choice <= places.size()) {
                    while (arrivaltime == null) {
                        try {
                            System.out.println("Enter Arrival Time in format 31.12.2013 15:23: ");
                            arrivaltime = CConstants.dateFormat.parse(new Scanner(System.in).nextLine());
                        } catch (ParseException e) {
                            System.err.println("Wrong Dateformat");
                        }
                    }
                    while (departuretime == null) {
                        try {
                            System.out.println("Enter Departure Time in format 31.12.2013 15:23: ");
                            departuretime = CConstants.dateFormat.parse(new Scanner(System.in).nextLine());
                        } catch (ParseException e) {
                            System.err.println("Wrong Dateformat");
                        }
                    }
                    stations.add(new CStationDetails(choice, arrivaltime, departuretime));
                }
            } catch (NumberFormatException ex) {
                choice=-1;
            }
        } while (choice != 0);
        return stations;
    }

    /**
     * prints the Start and End of all tours with times.
     * @param tours
     */
    public static void listTours(HashMap<Integer, CTourDetails> tours) {
        for (Map.Entry<Integer, CTourDetails> tour : tours.entrySet()) {
            System.out.printf("Tour %d:", tour.getKey());
            tour.getValue().print();
        }


    }
}
