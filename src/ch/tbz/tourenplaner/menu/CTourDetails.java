package ch.tbz.tourenplaner.menu;

import java.util.Date;

/**
 * Just a class to hold the start and end place of a tour used by CMenu to display the list if tours
 */
public class CTourDetails {
    private final String startplacename;
    private final String endplacename;
    private final Date arrivaltime;
    private final Date departuretime;

    public CTourDetails(String startplacename, Date departuretime, String endplacename, Date arrivaltime) {
        this.startplacename = startplacename;
        this.endplacename = endplacename;
        this.arrivaltime = arrivaltime;
        this.departuretime = departuretime;
    }

    public void print() {
        System.out.printf("Abfahrt:%s von:%s Ankunft:%s in:%s\n", CConstants.dateFormat.format(departuretime), startplacename, CConstants.dateFormat.format(arrivaltime), endplacename);
    }
}
