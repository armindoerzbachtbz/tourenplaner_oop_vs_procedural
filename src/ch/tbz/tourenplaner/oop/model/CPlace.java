package ch.tbz.tourenplaner.oop.model;

/**
 * This is the class storing the data of a place
 */
public class CPlace {
    private final String plz;
    private final String name;

    public CPlace(String plz, String name) {
        this.plz = plz;
        this.name = name;
    }

    public String getPlz() {
        return plz;
    }

    public String getName() {
        return name;
    }
}
