package ch.tbz.tourenplaner.oop.model;

import java.util.Date;
/**
 * This is the class storing the data of a station
 */

public class CStation {
    private CPlace place;
    private java.util.Date arrivaltime;
    private java.util.Date departuretime;

    public CStation(CPlace place, Date arrivaltime, Date departuretime) {
        this.arrivaltime = arrivaltime;
        this.departuretime = departuretime;
        this.place = place;
    }

    public CPlace getPlace() {
        return place;
    }

    public Date getArrivaltime() {
        return arrivaltime;
    }

    public Date getDeparturetime() {
        return departuretime;
    }
}
