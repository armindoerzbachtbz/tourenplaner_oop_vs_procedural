package ch.tbz.tourenplaner.oop.model;

import java.util.ArrayList;

/**
 * This class holds the data for a tour and modifies the tour
 */
public class CTour {
    private final ArrayList<CStation> stations = new ArrayList<>();

    public void addStation(CStation station) {
        stations.add(station);
    }

    public CStation getFirstStation() {
        return stations.get(0);
    }

    public CStation getLastStation() {
        return stations.get(stations.size() - 1);
    }
}
