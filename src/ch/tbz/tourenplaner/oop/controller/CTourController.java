package ch.tbz.tourenplaner.oop.controller;

import ch.tbz.tourenplaner.menu.CStationDetails;
import ch.tbz.tourenplaner.menu.CTourDetails;
import ch.tbz.tourenplaner.oop.model.CStation;
import ch.tbz.tourenplaner.oop.model.CTour;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class is used to add and modify tours.
 */
public class CTourController {
    /**
     * a Singleton Object of this class to store the pointers to all tours
     */
    static final private CTourController tourController = new CTourController();
    ArrayList<CTour> tours = new ArrayList<>();

    /**
     * This adds a tour with the stations
     * @param stations
     */
    public static void addTour(ArrayList<CStationDetails> stations) {
        CTour tour = new CTour();
        for (CStationDetails station : stations) {
            tour.addStation(new CStation(CPlaceController.getPlace(station.getPlace() - 1),
                    station.getArrivaltime(), station.getDeparturetime()));
        }
        tourController.tours.add(tour);
    }

    /**
     * This method returns a Hashmap of all Tours with CTourDetails for the CMenu class to display the tours.
     * @return Hashmap of all Tours with CTourDetails
     */
    public static HashMap<Integer, CTourDetails> getTours() {
        HashMap<Integer, CTourDetails> hashmap = new HashMap<>();
        for (int i = 0; i < CTourController.tourController.tours.size(); i++) {
            CTour tour = CTourController.tourController.tours.get(i);
            hashmap.put(i, new CTourDetails(tour.getFirstStation().getPlace().getName(),
                    tour.getFirstStation().getDeparturetime(), tour.getLastStation().getPlace().getName(), tour.getLastStation().getArrivaltime()));
        }
        return hashmap;
    }

}
