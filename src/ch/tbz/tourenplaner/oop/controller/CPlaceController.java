package ch.tbz.tourenplaner.oop.controller;

import ch.tbz.tourenplaner.oop.model.CPlace;

import java.util.ArrayList;

/**
 * This class is controlling the modifications of places
 */
public class CPlaceController {
    /**
     * a singleton Object of this class which is used to store the pointer to all places
     */
    static final private CPlaceController placeController = new CPlaceController();
    private final ArrayList<CPlace> places = new ArrayList<>();

    /**
     * Adds a new place
     * @param plz postal code
     * @param name name of town
     */
    public static void addPlace(String plz, String name) {
        placeController.places.add(new CPlace(plz, name));
    }

    /**
     * Returns the places to CMenu in suitable format
     * @return ArrayList if String[]
     */
    public static ArrayList<String[]> getPlaces() {
        ArrayList<String[]> list = new ArrayList<>();
        for (CPlace place : placeController.places) {
            String[] pl = {place.getPlz(), place.getName()};
            list.add(pl);
        }
        return list;

    }

    /**
     * Returns the place at index key
     * @param key index into arraylist places
     * @return CPlace object
     */
    public static CPlace getPlace(Integer key) {
        return placeController.places.get(key);
    }
}
