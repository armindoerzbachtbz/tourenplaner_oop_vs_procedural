package ch.tbz.tourenplaner;


import ch.tbz.tourenplaner.menu.CMenu;
import ch.tbz.tourenplaner.menu.CStationDetails;
import ch.tbz.tourenplaner.menu.CTourDetails;
import ch.tbz.tourenplaner.procedural.Structs.SPlace;
import ch.tbz.tourenplaner.procedural.Structs.SStation;
import ch.tbz.tourenplaner.procedural.Structs.STour;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Procedural program version of the tourenplaner
 */
public class CTourenplanerProcedural {
    /**
     * Datastore for tours
     */
    static ArrayList<STour> tours = new ArrayList<>();
    /**
     * Datastore for places
     */
    static ArrayList<SPlace> places = new ArrayList<>();

    public static void main(String[] args) {
        int choice;
        while (((choice = CMenu.mainmenu()) != 0)) {
            switch (choice) {
                case 1:
                    String[] s = CMenu.addPlace();
                    addPlace(s[0], s[1]);
                    break;
                case 2:
                    addTour(CMenu.addTour(getPlaces()));
                    break;
                case 3:
                    CMenu.listTours(getTours());
                    break;

            }
        }
    }

    /**
     * generates a Hashmap with tour details
     * @return Hashmap of tour details
     */
    private static HashMap<Integer, CTourDetails> getTours() {
        HashMap<Integer, CTourDetails> tourDetailsHashMap = new HashMap<>();
        for (int i = 0; i < tours.size(); i++) {
            ArrayList<SStation> stations = tours.get(i).stations;
            tourDetailsHashMap.put(i,
                    new CTourDetails(stations.get(0).place.name,
                    stations.get(0).departuretime,
                    stations.get(stations.size() - 1).place.name,
                    stations.get(stations.size() - 1).arrivaltime)
            );
        }
        return tourDetailsHashMap;
    }

    /**
     * Creates a new tour with the stationDetails and adds the tour to the ArrayList tours.
     * @param stationDetails
     */
    private static void addTour(ArrayList<CStationDetails> stationDetails) {
        ArrayList<SStation> stations = new ArrayList<>();
        for (CStationDetails station : stationDetails) {
            SStation s = new SStation();
            s.place = places.get(station.place - 1);
            s.arrivaltime = station.arrivaltime;
            s.departuretime = station.departuretime;
            stations.add(s);
        }
        STour t = new STour();
        t.stations = stations;
        tours.add(t);
    }

    /**
     * generates a ArrayList of String[] with the plz and name of all SPlaces structures in ArrayList places.
     * @return
     */
    private static ArrayList<String[]> getPlaces() {
        ArrayList<String[]> retplaces = new ArrayList<>();
        for (SPlace s : places) {
            retplaces.add(new String[]{s.plz, s.name});
        }
        return retplaces;

    }

    /**
     * Creates a SPlace object with plz and name and adds this to the ArrayList places
     * @param plz
     * @param name
     */
    public static void addPlace(String plz, String name) {
        SPlace s = new SPlace();
        s.plz = plz;
        s.name = name;
        places.add(s);
    }

}
