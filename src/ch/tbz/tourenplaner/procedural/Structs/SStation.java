package ch.tbz.tourenplaner.procedural.Structs;

import java.util.Date;
/**
 * Just a datastructure for storing a station
 */

public class SStation {
    public SPlace place;
    public Date arrivaltime;
    public Date departuretime;
}
