package ch.tbz.tourenplaner;

import ch.tbz.tourenplaner.menu.CMenu;
import ch.tbz.tourenplaner.oop.controller.CPlaceController;
import ch.tbz.tourenplaner.oop.controller.CTourController;

/**
 * Object oriented version of the tourenplaner
 */
public class CTourenplanerOop {
    public CTourenplanerOop() {
    }

    public static void main(String[] args) {
        int choice;
        while (((choice = CMenu.mainmenu()) != 0)) {
            switch (choice) {
                case 1:
                    String[] s = CMenu.addPlace();
                    CPlaceController.addPlace(s[0], s[1]);
                    break;
                case 2:

                    CTourController.addTour(CMenu.addTour(CPlaceController.getPlaces()));
                    break;
                case 3:
                    CMenu.listTours(CTourController.getTours());
                    break;

            }
        }

    }

}
