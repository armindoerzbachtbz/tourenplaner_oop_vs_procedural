# tourenplaner_oop_vs_procedural

This project shows the difference between implementing in object oriented and procedural.

There are two programs which are doing exactly the same, one is object oriented the other is procedural.

There are two classes to start the programm

- ch.tbz.tourenplaner.CTourenplanerOop
- ch.tbz.tourenplaner.CTourenplanerProcedural


The program has the following functionality:

- One can add a places, where a bus tour can stop.
- One can add tours and specify the stations (places) of the tour with departure and arrival times.
- One can list all tours.

The programm is organized in 3 main packages

- ch.tbz.tourenplaner.oop: object oriented style library
- ch.tbz.tourenplaner.procedural.Structs: just the data structs as classes
- ch.tbz.tourenplaner.menu: organizing input / output of user for both coding styles.
